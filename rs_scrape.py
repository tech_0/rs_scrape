from bs4 import BeautifulSoup
from urllib.request import urlopen
import requests
import xlrd
import csv

def import_excel():
	book = xlrd.open_workbook("site_names.xlsx")
	sheet = book.sheet_by_index(0)
	list_j = []
	j=1
	for k in range(1,sheet.nrows):
	    list_j.append(str(sheet.row_values(k)[j-1]))
	return list_j


def get_page(new_url):
	page = urlopen(new_url)
	soup = BeautifulSoup(page,'html.parser')
	return(soup)

def scrape_home(new_url):
	soup=get_page(new_url)
	contact = soup.find('div', {'class': 'phone-popup'})
	#print(contact)
	phone = contact.find('div', {'class': 'inner'})
	phone = phone.find('a', href=True)
	return((phone.text).strip())

def scrape(new_url):
	soup=get_page(new_url)
	mylists = soup.find('ul', {'class': 'realtor-list small-block-grid-2 medium-block-grid-3'})
	lis = mylists.find_all('li')
	member = lis[0]
	anchor = member.find('a', href=True)
	new_url="http://www."+url+anchor['href']
	page = urlopen(new_url)
	soup = BeautifulSoup(page,'html.parser')
	agent_name = soup.find('h2', {'class': 'agent-name'})
	contact = soup.find('div', {'class': 'contact-detail'})
	phone = contact.find('dd', {'class': 'phone_number'})
	email = contact.find('a', href=True)
	if (agent_name==None):
		name=None
	else:
		name=agent_name.text
	if (phone==None):
		phone=None
	else:
		phone = phone.text
	if (email==None):
		email=None
	else:
		email=email['href'][7:]
	return(name,phone,email)

urls=import_excel()
print(len(urls))
with open('result.csv','w',newline='') as f:
	write=csv.writer(f)
	write.writerow(['Site Name','Home Phone','Agent Name','Agent Phone','Agent Email'])
	count,agent_info_count,only_home_info_count=0,0,0
	for url in urls:
		count+=1
		site_name,home_phone,agent_name,agent_phone,agent_email=url,None,None,None,None
		new_url = "http://www."+url
		try:
			home_phone = scrape_home(new_url)
		except:
			continue
		
		new_url+="/about"
		try:
			agent_name,agent_phone,agent_email=scrape(new_url)
		except:
			temp=None
			#for specfic sites with aout-team url
			'''
			try:
				new_url = "http://www."+url+"/about-team"
				scrape(new_url)
			except:
				print("----------",url)
			'''	
		if (agent_name==None and not home_phone==None):
			only_home_info_count+=1
		elif (not agent_name==None):
			agent_info_count+=1
		print(count,",",site_name,",",home_phone,",",agent_name,",",agent_phone,",",agent_email)
		print('\n')
		try:
			write.writerow([site_name,home_phone,agent_name,agent_phone,agent_email])
		except:
			print("ERROR",count,site_name)
print("\n",count,only_home_info_count,agent_info_count)
